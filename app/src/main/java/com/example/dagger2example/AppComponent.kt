package com.example.dagger2example

import com.example.dagger2example.fragments.FirstFragment
import com.example.dagger2example.fragments.SecondFragment
import com.example.dagger2example.fragments.ThirdFragment
import com.example.dagger2example.modules.ApplicationModule
import com.example.dagger2example.modules.ThirdRepoModule
import dagger.Component
import javax.inject.Singleton

@Component(
    modules = [
        ApplicationModule::class,
        ThirdRepoModule::class
    ]
)
@Singleton
interface AppComponent {

    fun inject(mainActivity: MainActivity)
    fun inject(firstFragment: FirstFragment)
    fun inject(secondFragment: SecondFragment)
    fun inject(thirdFragment: ThirdFragment)

}
