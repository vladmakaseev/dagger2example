package com.example.dagger2example.modules

import com.example.dagger2example.repositories.IThirdRepo
import com.example.dagger2example.repositories.ThirdRepo
import dagger.Binds
import dagger.Module

@Module
interface ThirdRepoModule {

    /* Binds Помогает работать с интерфейсами-контрактами */
    @Binds
    fun provideThirdRepo(thirdRepo: ThirdRepo): IThirdRepo

}