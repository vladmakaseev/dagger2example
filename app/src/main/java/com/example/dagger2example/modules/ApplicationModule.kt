package com.example.dagger2example.modules

import com.example.dagger2example.fragments.SecondFragment
import com.example.dagger2example.repositories.SomeRepo
import com.example.dagger2example.repositories.SomeSecondRepo
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule {

    @Provides
    @Singleton
    fun provideSomeRepo() = SomeRepo()

    /*
        Особенность матода:
    - Обязательный возвращаемый тип данных
    - Предоставляет реализацию того, как именно предоставляется экземпляр
    - Аннотация по сути определяет ограничения на экземпляр, т.к.
     @Bind может вернуть условный интерфейс и соответственно не нужно будет дублировать одни и тот же код

         Аннотация создает специальный класс some_factory который,
    в свою очередь и не дает возможность возвращать абстрактные объекты и интерфейсы.
    */

    @Provides
    fun provideSecondFragment(someSecondRepo: SomeSecondRepo) = SecondFragment(someSecondRepo)

}