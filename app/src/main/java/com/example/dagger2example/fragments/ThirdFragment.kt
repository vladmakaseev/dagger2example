package com.example.dagger2example.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.dagger2example.App
import com.example.dagger2example.R
import com.example.dagger2example.repositories.ThirdRepo
import kotlinx.android.synthetic.main.fragment_third.*
import javax.inject.Inject

class ThirdFragment @Inject constructor(
    private val thirdRepo: ThirdRepo
    ) : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.appComponent.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_third, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViews()
    }

    private fun setupViews() {
        btnShowText.setOnClickListener {
            tvText.text = thirdRepo.getData()
        }
    }

}