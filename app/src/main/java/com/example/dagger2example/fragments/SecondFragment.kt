package com.example.dagger2example.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.dagger2example.*
import com.example.dagger2example.repositories.SomeSecondRepo
import com.example.dagger2example.repositories.ThirdRepo
import kotlinx.android.synthetic.main.fragment_second.*
import javax.inject.Inject

class SecondFragment @Inject constructor(
    private val someSecondRepo: SomeSecondRepo
) : Fragment() {

    @Inject lateinit var thirdRepo: ThirdRepo

    init {
        App.appComponent.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_second, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViews()
    }

    private fun setupViews() {
        btnShowSecondText.setOnClickListener {
            tvText.text = someSecondRepo.getSomeSecondRepo()
        }

        btnNextFragment.setOnClickListener {
            (activity as MainActivity)
                .supportFragmentManager
                .beginTransaction()
                .addToBackStack(null)
                .replace(R.id.vgContainer, ThirdFragment(thirdRepo))
                .commit()
        }
    }

}