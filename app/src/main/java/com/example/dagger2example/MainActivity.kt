package com.example.dagger2example

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.dagger2example.fragments.FirstFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager.beginTransaction().add(R.id.vgContainer, FirstFragment()).commit()
    }

}