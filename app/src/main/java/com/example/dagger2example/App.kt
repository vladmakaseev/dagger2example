package com.example.dagger2example

import android.app.Application

class App : Application() {

    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()

        setupDagger2()
    }

    private fun setupDagger2() {
        appComponent = DaggerAppComponent.builder().build()
    }

}