package com.example.dagger2example.repositories

import javax.inject.Inject

class SomeSecondRepo @Inject constructor() {

    fun getSomeSecondRepo() = "Some second repo data"

}