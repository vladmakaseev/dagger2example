package com.example.dagger2example.repositories

import javax.inject.Inject

class ThirdRepo @Inject constructor() : IThirdRepo {

    fun getData() = "Get last third data"
}

interface IThirdRepo